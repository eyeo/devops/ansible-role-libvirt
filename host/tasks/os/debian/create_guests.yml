# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---
# We use this include_tasks just as a wrapper, since "block:"
# doesn't support loops
# https://docs.ansible.com/ansible/latest/include_tasks_module.html
- include_tasks:
    file: "create_guests_tasks.yml"
    apply:
      become:
        true
  vars:
    guest_debootstrap_release:
      "{{ guest.value.debootstrap_release
        | default(guest.value.debootstrap_distribution)
        | default(libvirt_guests_debootstrap_release)
        | mandatory }}"
    guest_debootstrap_mirror:
      "{{ guest.value.debootstrap_mirror
        | default(libvirt_guests_debootstrap_mirror)
        | default('http://deb.debian.org/debian') }}"
    guest_virt_type:
      "{{ guest.value.virt_type
        | default(libvirt_guests_virt_type)
        | mandatory }}"
    guest_name:
      "{{ guest.value.name | default(guest.key) }}"
    guest_memory:
      "{{ (guest.value.ram | mandatory | last == 'G')
        | ternary(
            guest.value.ram | replace('G', '') | int * 1024,
            guest.value.ram
          )
        }}"
    guest_cpu:
      "{{ guest.value.cpu
        | default(libvirt_guests_cpu)
        | default('host-passthrough') }}"
    guest_bridge:
      "{{ guest.value.bridge
        | default(libvirt_guests_bridge)
        | mandatory }}"
    guest_start:
      "{{ guest.value.start | default(True) }}"
    guest_autostart:
      "{{ guest.value.autostart | default(True) }}"
    root_logical_volume_name:
      "{{ guest.value.root_logical_volume_name
        | default(guest_name) }}-root"
    root_logical_volume_size:
      "{{ guest.value.root_logical_volume_size
        | default(guest.value.logical_volume_size)
        | mandatory }}"
    root_volume_group:
      "{{ guest.value.root_volume_group
        | default(libvirt_guests_volume_group)
        | mandatory }}"
    root_disk_path:
      "/dev/{{ root_volume_group }}/{{ root_logical_volume_name }}"
    root_user_password:
      "{{ lookup('password', '/dev/null length=42') }}"
    root_authorized_keys:
      "{{ guest.value.authorized_keys
        | default(libvirt_guests_authorized_keys)
        | mandatory }}"
    debootstrap_packages_list:
      "{{ guest.value.debootstrap_packages
        | default(libvirt_guests_debootstrap_packages)
        | mandatory }}"
    guest_firstboot_commands:
      "{{ guest.value.firstboot_commands
        | default(libvirt_guests_firstboot_commands)
        | default([]) }}"
    vmdebootstrap_logdir:
      "/var/log/vmdebootstrap"
  when:
    "guest.value.name is not defined or guest.value.name != omit"
  loop:
    "{{ libvirt_guests | default({}) | dict2items }}"
  loop_control:
    label: "{{ guest.key }}"
    loop_var: "guest"
